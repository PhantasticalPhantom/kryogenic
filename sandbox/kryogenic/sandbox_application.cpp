#include "sandbox_application.hpp"

#include "kryogenic/ecs/components/transform.hpp"

namespace kryogenic {
	sandbox_application::sandbox_application() noexcept {
		auto&      scene_mngr = get_scene_mngr();
		auto const main_scene = scene_mngr.create_scene("main", true);
		auto const entity     = main_scene->create_entity("entity");
		entity->add_component<transform>();
	}
} // kryogenic
