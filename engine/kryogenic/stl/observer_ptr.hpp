#ifndef KRYOGENIC_STL_OBSERVER_PTR_HPP
#define KRYOGENIC_STL_OBSERVER_PTR_HPP

#include <memory>

namespace kryogenic {
	template<class T>
	class observer_ptr {
	public:
		using element_type = T;

		constexpr observer_ptr() noexcept = default;

		constexpr explicit observer_ptr(std::nullptr_t) noexcept {
		}

		constexpr observer_ptr(observer_ptr const& other) noexcept = default;
		constexpr observer_ptr(observer_ptr&& other) noexcept      = default;

		constexpr explicit observer_ptr(T* ptr) noexcept
			: m_ptr(ptr) {
		}

		constexpr explicit observer_ptr(std::shared_ptr<T> const& ptr) noexcept
			: m_ptr(ptr.get()) {
		}

		constexpr auto operator=(observer_ptr const& other) noexcept -> observer_ptr& = default;
		constexpr auto operator=(observer_ptr&& other) noexcept -> observer_ptr&      = default;

		constexpr auto operator=(T* ptr) noexcept -> observer_ptr& {
			m_ptr = ptr;
			return *this;
		}

		constexpr auto operator=(std::shared_ptr<T> const& ptr) noexcept -> observer_ptr& {
			m_ptr = ptr.get();
			return *this;
		}

		constexpr auto operator*() const noexcept -> T& {
			return *m_ptr;
		}

		constexpr auto operator->() const noexcept -> T* {
			return m_ptr;
		}

		constexpr auto operator[](std::ptrdiff_t idx) const noexcept -> T& {
			return m_ptr[idx];
		}

		constexpr auto get() const noexcept -> T* {
			return m_ptr;
		}

		constexpr explicit operator bool() const noexcept {
			return m_ptr != nullptr;
		}

		constexpr auto release() noexcept -> T* {
			auto ptr = m_ptr;
			m_ptr    = nullptr;
			return ptr;
		}

		constexpr auto reset() noexcept -> void {
			m_ptr = nullptr;
		}

		constexpr auto reset(T* ptr) noexcept -> void {
			m_ptr = ptr;
		}

		constexpr auto reset(std::shared_ptr<T> const& ptr) noexcept -> void {
			m_ptr = ptr.get();
		}

		friend auto swap(observer_ptr& lhs, observer_ptr& rhs) noexcept -> void {
			using std::swap;
			swap(lhs.m_ptr, rhs.m_ptr);
		}

	private:
		T* m_ptr = nullptr;
	};

	template<class T>
	constexpr auto make_observer(T* ptr) noexcept -> observer_ptr<T> {
		return observer_ptr<T>(ptr);
	}
}

#endif //KRYOGENIC_STL_OBSERVER_PTR_HPP
