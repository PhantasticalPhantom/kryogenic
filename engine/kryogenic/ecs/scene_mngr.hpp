#ifndef KRYOGENIC_ECS_SCENE_MNGR_HPP
#define KRYOGENIC_ECS_SCENE_MNGR_HPP

#include <memory>

#include "kryogenic/common/types.hpp"
#include "kryogenic/ecs/scene.hpp"
#include "kryogenic/stl/observer_ptr.hpp"

namespace kryogenic {
	class scene_mngr {
	public:
		scene_mngr() noexcept  = default;
		~scene_mngr() noexcept = default;

		scene_mngr(scene_mngr const& other)                        = default;
		scene_mngr(scene_mngr&& other) noexcept                    = default;
		auto operator=(scene_mngr const& other) -> scene_mngr&     = default;
		auto operator=(scene_mngr&& other) noexcept -> scene_mngr& = default;

		[[nodiscard]] auto create_scene(string const& name, b8 make_active) -> observer_ptr<scene>;
		auto               destroy_scene(string const& name) -> void;
		[[nodiscard]] auto get_scene(string const& name) const -> observer_ptr<scene>;
		[[nodiscard]] auto switch_scene(string const& name) -> observer_ptr<scene>;

		[[nodiscard]] auto active_scene() const -> observer_ptr<scene>;
		[[nodiscard]] auto scenes() const -> vector<std::unique_ptr<scene>> const&;

	private:
		observer_ptr<scene>            m_active_scene = {};
		vector<std::unique_ptr<scene>> m_scenes       = {};
	};
} // kryogenic

#endif //KRYOGENIC_ECS_SCENE_MNGR_HPP
