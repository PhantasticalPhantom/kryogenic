#ifndef KRYOGENIC_ECS_SCENE_HPP
#define KRYOGENIC_ECS_SCENE_HPP

#include "kryogenic/common/types.hpp"
#include "kryogenic/ecs/entity.hpp"
#include "kryogenic/stl/observer_ptr.hpp"

namespace kryogenic {
	class scene {
	public:
		scene() noexcept  = default;
		~scene() noexcept = default;

		explicit scene(string name) noexcept;

		scene(scene const& other)                        = default;
		scene(scene&& other) noexcept                    = default;
		auto operator=(scene const& other) -> scene&     = default;
		auto operator=(scene&& other) noexcept -> scene& = default;

		friend auto operator==(scene const& lhs, scene const& rhs) -> bool {
			return lhs.m_name == rhs.m_name;
		}

		friend auto operator!=(scene const& lhs, scene const& rhs) -> bool {
			return !(lhs == rhs);
		}

		auto create_entity(const string& name) -> observer_ptr<entity>;
		auto destroy_entity(string name) -> void;
		auto destroy_entity(observer_ptr<entity> entity) -> void;

		[[nodiscard]] auto get_entity(string name) -> observer_ptr<entity>;

		[[nodiscard]] auto get_name() const -> string;
		[[nodiscard]] auto get_entities() const -> vector<entity> const&;

	private:
		string         m_name     = {};
		vector<entity> m_entities = {};
	};
} // kryogenic

#endif //KRYOGENIC_ECS_SCENE_HPP
