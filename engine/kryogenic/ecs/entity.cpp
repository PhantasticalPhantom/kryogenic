#include "entity.hpp"

#include <utility>

namespace kryogenic {
	entity::entity(string name)
		: m_name(std::move(name)) {
	}

	entity::entity(observer_ptr<entity> const parent, string name)
		: m_name(std::move(name))
		, m_parent(parent) {
	}

	auto entity::remove_all_components() -> void {
		m_components.clear();
	}

	auto entity::add_child(std::unique_ptr<entity> child) -> void {
		child->m_parent = this;
		m_children.push_back(std::move(child));
	}

	auto entity::remove_child(entity const& child) -> void {
		auto const result = std::ranges::find_if(m_children, [&child](auto const& c) {
			return c.get() == &child;
		});

		if (result != m_children.end()) {
			m_children.erase(result);
		}
	}

	auto entity::remove_all_children() -> void {
		m_children.clear();
	}

	auto entity::get_name() const -> string {
		return m_name;
	}

	auto entity::get_parent() const -> observer_ptr<entity> {
		return m_parent;
	}

	auto entity::get_children() const -> vector<std::unique_ptr<entity>> const& {
		return m_children;
	}

	auto entity::get_components() const -> vector<std::unique_ptr<component_base>> const& {
		return m_components;
	}
} // kryogenic
