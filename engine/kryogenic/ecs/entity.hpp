#ifndef KRYOGENIC_ECS_ENTITY_HPP
#define KRYOGENIC_ECS_ENTITY_HPP

#include <algorithm>
#include <memory>
#include <vector>

#include "kryogenic/common/types.hpp"
#include "kryogenic/ecs/component_base.hpp"
#include "kryogenic/stl/observer_ptr.hpp"

namespace kryogenic {
	class entity {
	public:
		entity() noexcept  = default;
		~entity() noexcept = default;

		explicit entity(string name);
		explicit entity(observer_ptr<entity> parent, string name);

		entity(entity const& other)                        = delete;
		entity(entity&& other) noexcept                    = default;
		auto operator=(entity const& other) -> entity&     = delete;
		auto operator=(entity&& other) noexcept -> entity& = default;

		template<class T> requires std::derived_from<T, component_base>
		[[nodiscard]] auto get_component() -> observer_ptr<T> {
			auto const result = std::ranges::find_if(m_components, [](auto const& component) {
				return component->type == typeid(T);
			});

			if (result != m_components.end()) {
				return observer_ptr<T>(static_cast<T*>(result->get()));
			}

			return observer_ptr<T>();
		}

		template<class T>
		[[nodiscard]] auto has_component() const -> bool {
			return std::ranges::any_of(m_components, [](auto const& component) {
				return component->type == typeid(T);
			});
		}

		template<class T>
		auto add_component(std::unique_ptr<T> component) -> void {
			component->type   = typeid(T);
			component->parent = this;
			m_components.push_back(std::move(component));
		}

		template<class T>
		auto add_component() -> void {
			add_component(std::make_unique<T>());
		}

		template<class T, class... Args>
		auto add_component(Args&&... args) -> void {
			add_component(std::make_unique<T>(std::forward<Args>(args)...));
		}

		template<class T>
		auto remove_component() -> void {
			auto const result = std::ranges::find_if(m_components, [](auto const& component) {
				return component->type == typeid(T);
			});

			if (result != m_components.end()) {
				m_components.erase(result);
			}
		}

		auto remove_all_components() -> void;

		auto add_child(std::unique_ptr<entity> child) -> void;
		auto remove_child(entity const& child) -> void;
		auto remove_all_children() -> void;

		[[nodiscard]] auto get_name() const -> string;
		[[nodiscard]] auto get_parent() const -> observer_ptr<entity>;
		[[nodiscard]] auto get_children() const -> vector<std::unique_ptr<entity>> const&;
		[[nodiscard]] auto get_components() const -> vector<std::unique_ptr<component_base>> const&;

	private:
		string m_name = {};

		observer_ptr<entity>                    m_parent     = {};
		vector<std::unique_ptr<entity>>         m_children   = {};
		vector<std::unique_ptr<component_base>> m_components = {};
	};
} // kryogenic

#endif //KRYOGENIC_ECS_ENTITY_HPP
