#include "scene_mngr.hpp"

namespace kryogenic {
	auto scene_mngr::create_scene(string const& name, b8 make_active) -> observer_ptr<scene> {
		auto new_scene = std::make_unique<scene>(name);
		m_scenes.push_back(std::move(new_scene));
		auto const result = m_scenes.back().get();

		if (make_active) {
			m_active_scene = make_observer(result);
		}

		return make_observer(result);
	}

	auto scene_mngr::destroy_scene(string const& name) -> void {
		auto const result = std::ranges::find_if(m_scenes, [&name](auto const& s) {
			return s->get_name() == name;
		});

		if (result != m_scenes.end()) {
			m_scenes.erase(result);
		}
	}

	auto scene_mngr::get_scene(string const& name) const -> observer_ptr<scene> {
		auto const result = std::ranges::find_if(m_scenes, [&name](auto const& s) {
			return s->get_name() == name;
		});

		if (result != m_scenes.end()) {
			return make_observer(result->get());
		}

		return {};
	}

	auto scene_mngr::switch_scene(string const& name) -> observer_ptr<scene> {
		auto const result = std::ranges::find_if(m_scenes, [&name](auto const& s) {
			return s->get_name() == name;
		});

		if (result != m_scenes.end()) {
			m_active_scene = make_observer(result->get());
			return m_active_scene;
		}

		return {};
	}

	auto scene_mngr::active_scene() const -> observer_ptr<scene> {
		return m_active_scene;
	}

	auto scene_mngr::scenes() const -> vector<std::unique_ptr<scene>> const& {
		return m_scenes;
	}
} // kryogenic
