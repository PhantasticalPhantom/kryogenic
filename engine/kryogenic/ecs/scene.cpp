#include "scene.hpp"

#include "kryogenic/ecs/entity.hpp"

namespace kryogenic {
	scene::scene(string name) noexcept
		: m_name(std::move(name)) {
	}

	auto scene::create_entity(const string& name) -> observer_ptr<entity> {
		m_entities.emplace_back(name);
		return make_observer(&m_entities.back());
	}

	auto scene::destroy_entity(string name) -> void {
		auto const result = std::ranges::find_if(m_entities, [&name](auto const& e) {
			return e.get_name() == name;
		});

		if (result != m_entities.end()) {
			m_entities.erase(result);
		}
	}

	auto scene::destroy_entity(observer_ptr<entity> entity) -> void {
		auto const result = std::ranges::find_if(m_entities, [&entity](auto const& e) {
			return &e == entity.get();
		});

		if (result != m_entities.end()) {
			m_entities.erase(result);
		}
	}

	[[nodiscard]]

	auto scene::get_entity(string name) -> observer_ptr<entity> {
		auto const result = std::ranges::find_if(m_entities, [&name](auto const& e) {
			return e.get_name() == name;
		});

		if (result != m_entities.end()) {
			return make_observer(&(*result));
		}

		return {};
	}

	auto scene::get_name() const -> string {
		return m_name;
	}

	auto scene::get_entities() const -> vector<entity> const& {
		return m_entities;
	}
} // kryogenic
