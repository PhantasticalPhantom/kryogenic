#ifndef KRYOGENIC_ECS_TRANSFORM_HPP
#define KRYOGENIC_ECS_TRANSFORM_HPP

#include <glm/vec3.hpp>

#include "kryogenic/common/types.hpp"
#include "kryogenic/ecs/component_base.hpp"

namespace kryogenic {
	struct transform : component_base {
		fvec3 position = {};
		fvec3 rotation = {};
		fvec3 scale    = {1.0f, 1.0f, 1.0f};

		transform() noexcept  = default;
		~transform() noexcept = default;

		explicit transform(fvec3 const scalar) noexcept
			: position(scalar)
			, rotation(scalar)
			, scale(scalar) {
		}

		explicit transform(fvec3 const position, fvec3 const rotation, fvec3 const scale) noexcept
			: position(position)
			, rotation(rotation)
			, scale(scale) {
		}

		transform(transform const& other)                        = default;
		transform(transform&& other) noexcept                    = default;
		auto operator=(transform const& other) -> transform&     = default;
		auto operator=(transform&& other) noexcept -> transform& = default;
	};
}

#endif //KRYOGENIC_ECS_TRANSFORM_HPP
