#ifndef KRYOGENIC_ECS_SPRITE_HPP
#define KRYOGENIC_ECS_SPRITE_HPP

#include "kryogenic/common/types.hpp"
#include "kryogenic/ecs/component_base.hpp"

namespace kryogenic {
	struct sprite : component_base {
		sprite() noexcept  = default;
		~sprite() noexcept = default;

		sprite(sprite const& other)                        = default;
		sprite(sprite&& other) noexcept                    = default;
		auto operator=(sprite const& other) -> sprite&     = default;
		auto operator=(sprite&& other) noexcept -> sprite& = default;
	};
} // namespace kryogenic

#endif //KRYOGENIC_ECS_SPRITE_HPP
