#ifndef KRYOGENIC_ECS_COMPONENT_HPP
#define KRYOGENIC_ECS_COMPONENT_HPP

#include <typeindex>

#include "kryogenic/stl/observer_ptr.hpp"

namespace kryogenic {
	class entity;

	struct component_base {
		std::type_index      type   = typeid(component_base);
		observer_ptr<entity> parent = {};

		component_base() noexcept  = default;
		~component_base() noexcept = default;

		component_base(component_base const& other)                        = default;
		component_base(component_base&& other) noexcept                    = default;
		auto operator=(component_base const& other) -> component_base&     = default;
		auto operator=(component_base&& other) noexcept -> component_base& = default;
	};
} // kryogenic

#endif //KRYOGENIC_ECS_COMPONENT_HPP
