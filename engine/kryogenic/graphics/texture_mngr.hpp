#ifndef KRYOGENIC_GRAPHICS_TEXTURE_MNGR_HPP
#define KRYOGENIC_GRAPHICS_TEXTURE_MNGR_HPP

#include <unordered_map>

#include "kryogenic/common/types.hpp"

namespace kryogenic {
	struct texture;

	using texture_id = u32;

	class texture_mngr {
	public:
		texture_mngr() noexcept  = default;
		~texture_mngr() noexcept = default;

		texture_mngr(texture_mngr const& other)                        = delete;
		texture_mngr(texture_mngr&& other) noexcept                    = delete;
		auto operator=(texture_mngr const& other) -> texture_mngr&     = delete;
		auto operator=(texture_mngr&& other) noexcept -> texture_mngr& = delete;

		[[nodiscard]] auto create_texture(string const& path) -> texture_id;

	private:
		unordered_map<string, texture> m_textures = {};
	};
} // kryogenic

#endif //KRYOGENIC_GRAPHICS_TEXTURE_MNGR_HPP
