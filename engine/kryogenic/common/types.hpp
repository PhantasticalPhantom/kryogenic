#ifndef KRYOGENIC_COMMON_TYPES_HPP
#define KRYOGENIC_COMMON_TYPES_HPP

#include <string>
#include <unordered_map>
#include <vector>
#include <glm/fwd.hpp>

using u8  = unsigned char;
using u16 = unsigned short;
using u32 = unsigned int;
using u64 = unsigned long long;

using i8  = signed char;
using i16 = signed short;
using i32 = signed int;
using i64 = signed long long;

using f32 = float;
using f64 = double;

using b8  = bool;
using b32 = u32;

using usize = u64;
using isize = i64;
using byte  = u8;

using cstring = char const*;

using string = std::string;

template<class T>
using vector = std::vector<T>;

template<class Key, class T>
using unordered_map = std::unordered_map<Key, T>;

using vec1  = glm::vec1;
using vec2  = glm::vec2;
using vec3  = glm::vec3;
using vec4  = glm::vec4;
using ivec1 = glm::ivec1;
using ivec2 = glm::ivec2;
using ivec3 = glm::ivec3;
using ivec4 = glm::ivec4;
using uvec1 = glm::uvec1;
using uvec2 = glm::uvec2;
using uvec3 = glm::uvec3;
using uvec4 = glm::uvec4;
using fvec1 = glm::fvec1;
using fvec2 = glm::fvec2;
using fvec3 = glm::fvec3;
using fvec4 = glm::fvec4;
using dvec1 = glm::dvec1;
using dvec2 = glm::dvec2;
using dvec3 = glm::dvec3;
using dvec4 = glm::dvec4;

static_assert(sizeof(u8) == 1, "u8 is not 1 byte wide.");
static_assert(sizeof(u16) == 2, "u16 is not 2 bytes wide.");
static_assert(sizeof(u32) == 4, "u32 is not 4 bytes wide.");
static_assert(sizeof(u64) == 8, "u64 is not 8 bytes wide.");
static_assert(sizeof(i8) == 1, "i8 is not 1 byte wide.");
static_assert(sizeof(i16) == 2, "i16 is not 2 bytes wide.");
static_assert(sizeof(i32) == 4, "i32 is not 4 bytes wide.");
static_assert(sizeof(i64) == 8, "i64 is not 8 bytes wide.");
static_assert(sizeof(f32) == 4, "f32 is not 4 bytes wide.");
static_assert(sizeof(f64) == 8, "f64 is not 8 bytes wide.");
static_assert(sizeof(b8) == 1, "b8 is not 1 byte wide.");

#endif //KRYOGENIC_COMMON_TYPES_HPP
