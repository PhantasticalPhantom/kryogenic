#ifndef KRYOGENIC_COMMON_PLATFORM_HPP
#define KRYOGENIC_COMMON_PLATFORM_HPP

// Platform detection
#if defined(_WIN32) || defined(__WIN32__) || defined(__WINDOWS__)
#	define KRYOGENIC_PLATFORM_WINDOWS
#elif defined(__linux__)
#	define KRYOGENIC_PLATFORM_LINUX
#elif defined(__APPLE__)
#	define KRYOGENIC_PLATFORM_APPLE
#else
#	error "Unsupported platform!"
#endif

// Compiler detection
#if defined(_MSC_VER)
#	define KRYOGENIC_COMPILER_MSVC
#elif defined(__clang__)
#	define KRYOGENIC_COMPILER_CLANG
#elif defined(__GNUC__) || defined(__GNUG__)
#	define KRYOGENIC_COMPILER_GNU
#else
#	error "Unsupported compiler!"
#endif

// Architecture detection
#if defined(_M_AMD64) || defined(__amd64__) || defined(__x86_64__)
#	define KRYOGENIC_ARCHITECTURE_X86_64
#elif defined(_M_IX86) || defined(__i386__)
#	define KRYOGENIC_ARCHITECTURE_X86
#elif defined(__aarch64__)
#	define KRYOGENIC_ARCHITECTURE_ARM64
#elif defined(__arm__) || defined(_M_ARM)
#	define KRYOGENIC_ARCHITECTURE_ARM
#else
#	error "Unsupported architecture!"
#endif

// Endianess detection
#if defined(KRYOGENIC_PLATFORM_WINDOWS)
#	define KRYOGENIC_ENDIANNESS_LITTLE
#elif defined(KRYOGENIC_PLATFORM_LINUX) || defined(KRYOGENIC_PLATFORM_APPLE)
#	include <endian.h>
#	if __BYTE_ORDER == __LITTLE_ENDIAN
#		define KRYOGENIC_ENDIANNESS_LITTLE
#	elif __BYTE_ORDER == __BIG_ENDIAN
#		define KRYOGENIC_ENDIANNESS_BIG
#	else
#		error "Unsupported endianness!"
#	endif
#else
#	error "Unsupported platform!"
#endif

#endif //KRYOGENIC_COMMON_PLATFORM_HPP
