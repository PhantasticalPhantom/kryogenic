#include "window.hpp"

#include <SDL3/SDL.h>
#include <SDL3/SDL_events.h>
#include <SDL3/SDL_video.h>

#include "log.hpp"

namespace kryogenic {
	window::window(string title, u16 const width, u16 const height)
		: m_width(width)
		, m_height(height)
		, m_title(std::move(title)) {
		if (SDL_Init(SDL_INIT_VIDEO != 0)) {
			error("Failed to initialize SDL: {}.", SDL_GetError());
			throw std::runtime_error("Failed to initialize SDL.");
		}

		m_native_window = SDL_CreateWindow(
			m_title.c_str(),
			m_width,
			m_height,
			SDL_WINDOW_RESIZABLE
			);

		if (!m_native_window) {
			error("Failed to create window: {}.", SDL_GetError());
			throw std::runtime_error("Failed to create window.");
		}

		info("Window created with title: {}, width: {}, height: {}.", m_title, m_width, m_height);
	}

	auto window::poll_events() -> void {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
				case SDL_EVENT_QUIT:
					m_is_closed = true;
					break;
				case SDL_EVENT_WINDOW_RESIZED:
					m_width = event.window.data1;
					m_height     = event.window.data2;
					m_is_resized = true;
					break;
				case SDL_EVENT_WINDOW_MINIMIZED:
					m_is_minimized = true;
					break;
				case SDL_EVENT_WINDOW_RESTORED:
					m_is_minimized = false;
					break;
				default: break;
			}
			break;
		}
	}

	auto window::get_native_window() const -> void* {
		return m_native_window;
	}

	auto window::get_width() const -> u16 {
		return m_width;
	}

	auto window::get_height() const -> u16 {
		return m_height;
	}

	auto window::get_title() const -> string {
		return m_title;
	}

	auto window::is_closed() const -> b8 {
		return m_is_closed;
	}

	auto window::is_resized() const -> b8 {
		return m_is_resized;
	}

	auto window::is_minimized() const -> b8 {
		return m_is_minimized;
	}
} // kryogenic
