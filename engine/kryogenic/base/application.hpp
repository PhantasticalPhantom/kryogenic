#ifndef KRYOGENIC_BASE_APPLICATION_H
#define KRYOGENIC_BASE_APPLICATION_H

#include <memory>

#include "kryogenic/base/window.hpp"
#include "kryogenic/ecs/scene_mngr.hpp"

namespace kryogenic {
	class application {
	public:
		application() noexcept;
		~application() noexcept = default;

		application(application const& other)                        = delete;
		application(application&& other) noexcept                    = delete;
		auto operator=(application const& other) -> application&     = delete;
		auto operator=(application&& other) noexcept -> application& = delete;

		auto execute() -> void;
		auto shutdown() -> void;

		[[nodiscard]] auto get_window() const -> window&;
		[[nodiscard]] auto get_scene_mngr() const -> scene_mngr&;

	private:
		std::unique_ptr<window>     m_window     = {};
		std::unique_ptr<scene_mngr> m_scene_mngr = {};
	};

	[[nodiscard]] inline auto create_application() -> application*;
	[[nodiscard]] auto        get_application() -> application&;
} // kryogenic

#endif //KRYOGENIC_BASE_APPLICATION_H
