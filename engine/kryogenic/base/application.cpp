#include "application.hpp"

#include "kryogenic/base/log.hpp"

namespace kryogenic {
	namespace {
		application* g_application = nullptr;
	}
}

namespace kryogenic {
	application::application() noexcept {
		g_application = this;

		m_window     = std::make_unique<window>("Kryogenic", 1280, 720);
		m_scene_mngr = std::make_unique<scene_mngr>();

		trace("Kryogenic has been successfully initialized! All aboard the freezing hype train!");
	}

	auto application::execute() -> void {
		while (!m_window->is_closed()) {
			m_window->poll_events();
		}
	}

	auto application::shutdown() -> void {
		m_window.reset();
	}

	auto application::get_window() const -> window& {
		return *m_window;
	}

	auto application::get_scene_mngr() const -> scene_mngr& {
		return *m_scene_mngr;
	}

	auto get_application() -> application& {
		if (!g_application) {
			throw std::runtime_error("No application instance has been created!");
		}

		return *g_application;
	}
} // kryogenic
