#ifndef KRYOGENIC_BASE_ENTRYPOINT_HPP
#define KRYOGENIC_BASE_ENTRYPOINT_HPP

#include <iostream>

#include "kryogenic/base/application.hpp"
#include "kryogenic/base/log.hpp"

// ReSharper disable once CppNonInlineFunctionDefinitionInHeaderFile
int main() {
	kryogenic::log_init();
	auto const application = kryogenic::create_application();
	application->execute();
	application->shutdown();
	delete application;
	kryogenic::log_fini();
}

#endif // KRYOGENIC_BASE_ENTRYPOINT_HPP
